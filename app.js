// const express = require('express');
// const app = express();
// const path = require("path");
// app.use(express.static(__dirname + '/dist'));
// app.get('/*', function(req, res) {
//   res.sendFile(path.join(__dirname + '/dist/index.html'));
// });

// app.listen(process.env.PORT || 8080);

const express = require('express');
const app = express();
const path = require("path");

app.use('/', function(req, res, next) {
    if (process.env.NODE_ENV === 'production') {
        if (req.headers['x-forwarded-proto'] != 'https') {
            return res.redirect('https://sepio-interface.herokuapp.com');
        } else {
            return next();
        }
    } else {
        return next();
    }
})

app.use(express.static(__dirname + '/dist'));
app.get('/*', function(req, res) {
    res.sendFile(path.join(__dirname + '/dist/index.html'));
});

app.listen(process.env.PORT || 8080);