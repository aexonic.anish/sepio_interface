import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MasterService } from '../services/master-service.service';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.css']
})
export class PageNotFoundComponent implements OnInit {

  constructor(private router: Router, private masterService: MasterService) { }

  ngOnInit() {
  }

  backToLogin() {
    this.masterService.removeToken();
    this.router.navigate(['/login']);
  }

}
