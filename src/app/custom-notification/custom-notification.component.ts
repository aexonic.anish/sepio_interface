import { Component, OnInit } from '@angular/core';
import { MasterService } from '../services/master-service.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-custom-notification',
  templateUrl: './custom-notification.component.html',
  styleUrls: ['./custom-notification.component.css']
})
export class CustomNotificationComponent implements OnInit {

  public selectedItemsCompanyName = [];
  public dropdownSettingsExporter = {};
  public companyName;
  public companyId;
  public companyList = [];
  public companyData = [];

  public selectedItemsDistributorName = [];
  public dropdownSettingsDistributor = {};
  public distributorName;
  public distributorId;
  public distributorList = [];
  public distributorData = [];

  public selectedItemsCustomsName = [];
  public dropdownSettingsCustoms = {};
  public customsName;
  public customsId;
  public customsList = [];
  public customsData = [];

  public selectedItemsInstalltionName = [];
  public dropdownSettingsInstallation = {};
  public installationCompanyName;
  public installationCompanyId;
  public installationCompanyList = [];
  public installationCompanyData = [];

  public selectedItemsExpViaDisName = [];
  public dropdownSettingsExpViaDis = {};
  public expViaDisName;
  public expViaDisId;
  public expViaDisList = [];
  public expViaDisData = [];

  public showExporterDropdown: boolean = false;
  public showDistributorDropdown: boolean = false;
  public showCustomsDropdown: boolean = false;
  public showInstallationDropdown: boolean = false;
  public showExpViaDisDropdown: boolean = false;
  public paginationLoader = false;

  public checkValue;
  public checkValuesArray = [];
  public checkStatus: boolean = false;
  public exporter;
  public distributor;
  public customs;
  public installationCheck;
  public expViaDis;
  public all;
  public checkDisable: boolean = false;
  public checkDisableALL: boolean = false;
  public checkDisableExpViaDis: boolean = false;
  public notificationHeading;
  public message;
  public checkValueSend = [];
  public valueSendAll;

  public sendId;
  public alertSuccess: boolean = false;
  public alertDanger: boolean = false;
  public successMsg;
  public errorMsg;

  constructor(private masterService: MasterService) { }

  ngOnInit() {
                this.dropdownSettingsExporter = {
                                                    singleSelection: true,
                                                    text: "Select Company",
                                                    enableCheckAll:false,
                                                    enableSearchFilter: true,
                                                    badgeShowLimit: 6,
                                                    classes: "myclass custom-class",
                                                    searchPlaceholderText: "Type here to search"
                                                };

                this.dropdownSettingsDistributor = {
                                                      singleSelection: true,
                                                      text: "Select Distributor",
                                                      enableCheckAll:false,
                                                      enableSearchFilter: true,
                                                      badgeShowLimit: 6,
                                                      classes: "myclass custom-class",
                                                      searchPlaceholderText: "Type here to search"
                                                };      
                                                
                this.dropdownSettingsCustoms = {
                                                  singleSelection: true,
                                                  text: "Select Customs",
                                                  enableCheckAll:false,
                                                  enableSearchFilter: true,
                                                  badgeShowLimit: 6,
                                                  classes: "myclass custom-class",
                                                  searchPlaceholderText: "Type here to search"
                                              };     
              this.dropdownSettingsInstallation = {
                                                    singleSelection: true,
                                                    text: "Select Company",
                                                    enableCheckAll:false,
                                                    enableSearchFilter: true,
                                                    badgeShowLimit: 6,
                                                    classes: "myclass custom-class",
                                                    searchPlaceholderText: "Type here to search"
                                                };     
              this.dropdownSettingsExpViaDis = {
                                                  singleSelection: true,
                                                  text: "Select Exporter Via Distributor",
                                                  enableCheckAll:false,
                                                  enableSearchFilter: true,
                                                  badgeShowLimit: 6,
                                                  classes: "myclass custom-class",
                                                  searchPlaceholderText: "Type here to search"
                                              };                                                 
    this.getCompanyList();                                                    
    this.getDistributorList();
    this.getCustomsList(); 
    this.getInstallationList();
    this.getExporterViaDistributorCompanyList();
  }

  //--------------- Exporter Dropdown --------------//
  onItemSelectCompany(item: any) 
  {
      this.companyName = item.id;
  }

  OnItemDeSelectCompany(item: any) 
  {
      this.companyName = null;
  } 

  getCompanyList()
	{
		this.masterService.getCompanyData()
		.subscribe(
                (data) => {
                            this.companyList = data["data"];
                            for(var i = 0; i < this.companyList.length; i++) 
                            {
                              this.companyData.push({id:this.companyList[i].companyId, itemName:this.companyList[i].companyName})
                            }
                        },
                (error) => {
                                            console.log(error); 
                      },
                () => {}
              );	
  }

  //--------------- Distributor Dropdown --------------//
  onItemSelectDistributor(item: any) 
  {
      this.distributorName = item.id;
      // console.log(this.distributorName);
  }

  OnItemDeSelectDistributor(item: any) 
  {
      this.distributorName = null;
  } 

  getDistributorList()
	{
		this.masterService.getDistributorData()
		.subscribe(
                (data) => {
                            this.distributorList = data["data"];
                            for(var i = 0; i < this.distributorList.length; i++) 
                            {
                              this.distributorData.push({id:this.distributorList[i].distributorId, itemName:this.distributorList[i].distributorId})
                            }
                        },
                (error) => {
                              console.log(error); 
                            },
                () => {}
              );	
  }

  //--------------- Customs Dropdown --------------//
  onItemSelectCustoms(item: any) 
  {
      this.customsName = item.itemName;
      // console.log(this.customsName);
  }

  OnItemDeSelectCustoms(item: any) 
  {
      this.customsName = null;
  } 

  getCustomsList()
	{
		this.masterService.getCustomsData()
		.subscribe(
                (data) => {
                            this.customsList = data["data"];
                            for(var i = 0; i < this.customsList.length; i++) 
                            {
                              this.customsData.push({id:this.customsList[i].userId, itemName:this.customsList[i].email})
                            }
                        },
                (error) => {
                              console.log(error); 
                            },
                () => {}
              );	
  }

   //--------------- Installation Dropdown --------------//
   onItemSelectInstallation(item: any) 
   {
       this.installationCompanyName = item.id;
      //  console.log(this.installationCompanyName);
   }
 
   OnItemDeSelectInstallation(item: any) 
   {
       this.installationCompanyName = null;
   } 
 
   getInstallationList()
   {
     this.masterService.getCompanyData()
     .subscribe(
                 (data) => {
                             this.installationCompanyList = data["data"];
                             for(var i = 0; i < this.installationCompanyList.length; i++) 
                             {
                               this.installationCompanyData.push({id:this.installationCompanyList[i].companyId, itemName:this.installationCompanyList[i].companyName})
                             }
                         },
                 (error) => {
                               console.log(error); 
                             },
                 () => {}
               );	
   }

   //--------------- Exporter Via Distributor Dropdown --------------//
   onItemSelectExpViaDis(item: any) 
  {
      this.expViaDisName = item.id;
  }

  OnItemDeSelectExpViaDis(item: any) 
  {
      this.expViaDisName = null;
  } 

  getExporterViaDistributorCompanyList()
	{
		this.masterService.getCompanyViaDisData()
		.subscribe(
                (data) => {
                            this.expViaDisList = data["data"];
                
                            for(var i = 0; i < this.expViaDisList.length; i++) 
                            {
                              this.expViaDisData.push({id:this.expViaDisList[i].companyId, itemName:this.expViaDisList[i].companyName})
                            }
                        },
                (error) => {
                              console.log(error); 
                            },
                () => {}
              );	
  }

   checkboxValue(e)
   {
      this.checkValue = e.target.value;

      this.checkStatus = e.target.checked;
      if(this.checkValue == "all" && this.checkStatus == true)
      {
          this.checkValuesArray = [];
          this.checkDisableExpViaDis = true;
          this.checkDisable = true;
          this.exporter = true;
          this.distributor = true;
          this.customs = true;
          // this.installationCheck = true; 
          $('#defaultCheck5').prop('checked', true);
      }
      else if(this.checkValue == "all" && this.checkStatus == false)
      {
          this.checkValuesArray = [];
          this.checkDisable = false;
          this.checkDisableExpViaDis = false;
          this.exporter = false;
          this.distributor = false;
          this.customs = false;
          // this.installationCheck = false;
          $('#defaultCheck5').prop('checked', false);
      }

      if(this.checkValue == 10 && this.checkStatus == true)
      {
          this.checkValuesArray = [];
          this.checkDisable = true;
          this.checkDisableALL = true;
      }
      else if(this.checkValue == 10 && this.checkStatus == false)
      {
          this.checkValuesArray = [];
          this.checkDisable = false;
          this.checkDisableALL = false; 
      }
    
      if((this.checkValue == "all"|| this.checkValue == 2 || this.checkValue == 3 || this.checkValue == 4 || this.checkValue == 7) && this.checkStatus == true)
      {
        this.checkDisableExpViaDis = true; 
      }
      else
      {
        this.checkDisableExpViaDis = false; 
      }

      if(this.checkStatus == true)
      {
          this.checkValuesArray.push(this.checkValue);           
          
          if(this.checkValuesArray.length > 1)
          {
                this.showExporterDropdown = false;
                this.showDistributorDropdown = false;
                this.showCustomsDropdown = false;
                this.showInstallationDropdown = false;
                // this.showExpViaDisDropdown = false;
          }
          else if(this.checkValuesArray.length == 1)
          {
                if(this.checkValuesArray[0] == 2)
                {
                  this.showExporterDropdown = true;
                  this.showDistributorDropdown = false;
                  this.showCustomsDropdown = false;
                  this.showInstallationDropdown = false;
                  // this.showExpViaDisDropdown = false;
                }
                else if(this.checkValuesArray[0] == 3)
                {
                  this.showExporterDropdown = false;
                  this.showDistributorDropdown = false;
                  this.showCustomsDropdown = false;
                  this.showInstallationDropdown = true;
                  // this.showExpViaDisDropdown = false;
                  this.installationCheck = true;
                  
                  $('#defaultCheck5').prop('checked', true);
                }
                else if(this.checkValuesArray[0] == 4)
                {
                  this.showExporterDropdown = false;
                  this.showDistributorDropdown = false;
                  this.showCustomsDropdown = true;
                  this.showInstallationDropdown = false;
                  // this.showExpViaDisDropdown = false;
                }
                else if(this.checkValuesArray[0] == 7)
                {
                  this.showExporterDropdown = false;
                  this.showDistributorDropdown = true;
                  this.showCustomsDropdown = false;
                  this.showInstallationDropdown = false;
                  // this.showExpViaDisDropdown = false;
                }
                else if(this.checkValuesArray[0] == 10)
                {
                  this.showExporterDropdown = false;
                  this.showDistributorDropdown = false;
                  this.showCustomsDropdown = false;
                  this.showInstallationDropdown = false;
                  // this.showExpViaDisDropdown = true;
                }
          }
          else if(this.checkValuesArray.length == 0)
          {
              this.showExporterDropdown = false;
              this.showDistributorDropdown = false;
              this.showCustomsDropdown = false;
              this.showInstallationDropdown = false;
              // this.showExpViaDisDropdown = false;
            }
          }
          else
          {
            var index = this.checkValuesArray.indexOf(this.checkValue);        
        if (index > -1)
        {
          this.checkValuesArray.splice(index, 1);
          if(this.checkValuesArray.length == 1)
          {
            this.checkDisableExpViaDis = true; 
          }
          else if(this.checkValuesArray.length > 1)
          {
            this.checkDisableExpViaDis = true;
          }
          else 
          {
            this.checkDisableExpViaDis = false; 
          }


          if(this.checkValuesArray.length > 1)
          {
                this.showExporterDropdown = false;
                this.showDistributorDropdown = false;
                this.showCustomsDropdown = false;
                this.showInstallationDropdown = false;
                // this.showExpViaDisDropdown = false;
          }
          else if(this.checkValuesArray.length == 1)
          {
                if(this.checkValuesArray[0] == 7)
                {
                  this.showExporterDropdown = false;
                  this.showDistributorDropdown = true;
                  this.showCustomsDropdown = false;
                  this.showInstallationDropdown = false;
                  // this.showExpViaDisDropdown = false;
                }
                else if(this.checkValuesArray[0] == 2)
                {
                  this.showExporterDropdown = true;
                  this.showDistributorDropdown = false;
                  this.showCustomsDropdown = false;
                  this.showInstallationDropdown = false;
                  // this.showExpViaDisDropdown = false;
                }
                else if(this.checkValuesArray[0] == 4)
                {
                  this.showExporterDropdown = false;
                  this.showDistributorDropdown = false;
                  this.showCustomsDropdown = true;
                  this.showInstallationDropdown = false;
                  // this.showExpViaDisDropdown = false;
                }
                else if(this.checkValuesArray[0] == 3)
                {
                  this.showExporterDropdown = false;
                  this.showDistributorDropdown = false;
                  this.showCustomsDropdown = false;
                  this.showInstallationDropdown = true;
                  // this.showExpViaDisDropdown = false;
                }
                else if(this.checkValuesArray[0] == 10)
                {
                  this.showExporterDropdown = false;
                  this.showDistributorDropdown = false;
                  this.showCustomsDropdown = false;
                  this.showInstallationDropdown = false;
                  // this.showExpViaDisDropdown = true;
                }
          }
          else if(this.checkValuesArray.length == 0)
          {
                  this.showExporterDropdown = false;
                  this.showDistributorDropdown = false;
                  this.showCustomsDropdown = false;
                  this.showInstallationDropdown = false;
                  // this.showExpViaDisDropdown = false;
          }
      }
     }
     
   }
   
   sendNotification()
   {
     if(this.checkValuesArray[0] == "all")
      {
       this.sendId = null;
      }
      else if(this.checkValuesArray[0] == 2 && this.companyName == undefined)
      {
        this.sendId = null;
      }
      else if(this.checkValuesArray[0] == 2)
      {
        this.sendId = this.companyName;
      }
      else if(this.checkValuesArray[0] == 3 && this.installationCompanyName == undefined)
      {
        this.sendId = null;
      }
      else if(this.checkValuesArray[0] == 3)
      {
        this.sendId = this.installationCompanyName;
      }
      else if(this.checkValuesArray[0] == 4 && this.customsName == undefined)
      {
        this.sendId = null;
      }
      else if(this.checkValuesArray[0] == 4)
      {
        this.sendId = this.customsName;
      }
      else if(this.checkValuesArray[0] == 7 && this.distributorName == undefined)
      {
        this.sendId = null;
      }
      else if(this.checkValuesArray[0] == 7)
      {
        this.sendId = this.distributorName;
      }
      
      if(this.checkValuesArray.length == 0)
      {
            this.alertDanger = true;
            this.errorMsg = "PLease select User Type";
            $(".alert-danger").fadeIn('fast');
            setTimeout(function() {
                $(".alert-danger").fadeOut(2000);
            }, 4000);
      }
      else
      {

        let customNotification = {
                                    "heading": this.notificationHeading,
                                    "message":this.message,
                                    "userType": this.checkValuesArray,
                                    "sendId": this.sendId
                                  }
        this.paginationLoader = true;
        this.masterService.sendCustomNotificationData(customNotification)
        .subscribe(
                      (data) => {
                                    this.paginationLoader = false;
                                    this.alertSuccess = true;
                                    this.successMsg = data["data"];
                                    $(".alert-success").fadeIn('fast');
                                    setTimeout(function() {
                                        $(".alert-success").fadeOut(2000);
                                    }, 4000);
                                    $("#heading").val(''); 
                                    $("#message").val('');
                                    location.reload(); 
                                    this.all = false;
                                    this.exporter = false;
                                    this.distributor = false;
                                    this.customs = false;
                                    this.installationCheck = false;
                                    this.expViaDis = false;

                                },
                    (error) =>  {
                                 this.paginationLoader = false;
                                  console.log(error); 
                                },
                    () => {}
                );	
      }
      
      
   }
}
