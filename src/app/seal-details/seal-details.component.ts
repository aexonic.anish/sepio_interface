import { Component, OnInit, ViewChild } from '@angular/core';
import { MasterService } from '../services/master-service.service';
import * as $ from 'jquery';
import { Router } from '@angular/router';
import { Daterangepicker } from 'ng2-daterangepicker';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { DaterangePickerComponent } from 'ng2-daterangepicker';
import { FormGroup, AbstractControl, FormBuilder, Validators, FormArray } from '@angular/forms';
// import { IMultiSelectOption, IMultiSelectTexts, IMultiSelectSettings } from 'angular-2-dropdown-multiselect';

declare var moment: any;

@Component({
    selector: 'app-seal-details',
    templateUrl: './seal-details.component.html',
    styleUrls: ['./seal-details.component.css']
})
export class SealDetailsComponent implements OnInit {

    @ViewChild("fileInput") fileInput;
    @ViewChild(DaterangePickerComponent)
    public picker: DaterangePickerComponent;

    public sealNumber;
    public successMsg;
    public errorMsg;
    public alertSuccess: boolean = false;
    public alertDanger: boolean = false;
    public sealDetails = [];
    public editSealDetails = [];
    public shippingBillNo = [];
    public shippingBillNo1 = [];

    public shippingBillDate = [];
    public shippingBillDate1 = [];
    public editBtnShow: boolean = true;
    public saveBtnShow: boolean = false;

    public sealingDateValue: any;
    public shippingBillDateValue: any;
    public sealNo: any;
    public sealingDate;
    public sealingTime;
    public truckNo;
    public containerNo;
    public confirmSealingDate;
    public convertDate;
    public shipBillNo: any = [];
    public editBtnDisabled: boolean = true;
    public hideSealDiv: boolean = false;
    public hideIconDiv: boolean = true;
    public showDiv: boolean = false;
    public scanSealShow: boolean = false;
    public saveBtnDisable: boolean = false;
    public searchBox: boolean = false;
    public paginationLoader: boolean = false;

    public count;
    public icdDropdown = [];
    public selectedItemsEditCompanyIcd = [];
    public dropdownSettingsSepioEditCompanyIcds = {};
    public editCompanyIcd = [];
    public portIcds = [];
    public getIcdsInDropdown = [];
    public icdValue;
    public getPortIcds = [];
    public array = []
    public selectedItemsEditIcdPort = [];
    public editIcdPort;


    constructor(private masterService: MasterService, private router: Router, private daterangepickerOptions: DaterangepickerConfig) {

        this.sealingDateValue = Date.now();
        this.shippingBillDateValue = Date.now();

        this.daterangepickerOptions.settings = {
            locale: { format: 'DD-MM-YYYY' }
        };

    }

    ngOnInit() {

        this.dropdownSettingsSepioEditCompanyIcds = {
            singleSelection: true,
            text: "Select ICD",
            enableCheckAll: false,
            enableSearchFilter: true,
            badgeShowLimit: 6,
            classes: "myclass custom-class",
            searchPlaceholderText: "Type here to search"
        };
    }

    public sealingDatePicker = {
        singleDatePicker: true,
        showDropdowns: true,
        opens: "left"
    }
    public shippingBillDatePicker = {
        singleDatePicker: true,
        showDropdowns: true,
        opens: "left"
    }

    public sealingDateSelect(value: any) {
        this.sealingDateValue = value.start;

    }

    onItemSelectEditCompanyIcd(item: any) {
        this.editIcdPort = this.selectedItemsEditIcdPort[0]["itemName"];
        // console.log(this.editIcdPort);
    }
    OnItemDeSelectEditCompanyIcd(item: any) {
        this.selectedItemsEditIcdPort;
    }



    checkValueTime() {
        if (this.sealingTime.length == 0) {
            $("#check_blank_time").show();
        }
        else {
            $("#check_blank_time").hide();
        }
    }
    checkValue() {

        if (this.containerNo.length < 11 && this.containerNo.length >= 1) {
            $("#check_con_password").show();
            $("#check_blank").show();
        }
        else if (this.containerNo.length == 0) {
            $("#check_blank").show();
        }
        else {
            $("#check_con_password").hide();
            $("#check_blank").hide();
        }
    }
    checkValueTruck() {
        if (this.truckNo.length == 0) {
            $("#check_blank_truck").show();
        }
        else {
            $("#check_blank_truck").hide();
        }
    }
    public shippingDateSelect(event, shipBillDate, i) {

        this.convert(event.end._d);
        var index = this.shippingBillDate.indexOf(shipBillDate);

        if (index !== -1) {
            this.shippingBillDate[index] = this.convertDate;
        }

    }


    public convert(str) {
        var date = new Date(str),
            mnth = ("0" + (date.getMonth() + 1)).slice(-2),
            day = ("0" + date.getDate()).slice(-2);
        this.convertDate = [day, mnth, date.getFullYear()].join("-")

    }

    editHide() {
        this.editBtnShow = false;
        this.saveBtnShow = true;

        this.searchBox = true;
        this.selectedItemsEditIcdPort = [];
        this.getIcdsInDropdown = [];

        var isSelected = false;
        for (var md1 = 0; md1 < this.icdDropdown.length; md1++) {
            for (var mf1 = 0; mf1 < this.array.length; mf1++) {
                if (this.icdDropdown[md1].itemName == this.array[mf1]) {
                    isSelected = true;
                    this.getIcdsInDropdown.push({ id: this.icdDropdown[md1].id, itemName: this.icdDropdown[md1].itemName })
                }

            }
            if (isSelected == false) {
                this.getIcdsInDropdown.push({ id: 1000, itemName: this.array[0] })
            }
        }
        // console.log(this.getIcdsInDropdown);
        this.selectedItemsEditIcdPort = this.getIcdsInDropdown;
        this.editIcdPort = this.selectedItemsEditIcdPort[0].itemName;

    }

    saveHide() {
        this.searchBox = false;
        this.saveSealDetail();


    }

    changedArray(event, i, shipBillNo) {

        var index = this.shippingBillNo.indexOf(shipBillNo);

        if (index !== -1) {
            this.shippingBillNo[index] = event.target.value;
        }
    }
    changedBillDate(event, i, shipBillDate) {

        ///////////////////THIS CODE IS FOR FIND VALUE IN PARTICULAR ARRY INDEX AND REPLACE VALUE////////////////
        var index = this.shippingBillDate.indexOf(shipBillDate);

        if (index !== -1) {
            this.shippingBillDate[index] = event.target.value;
        }

        /////////////////////////END//////////////////////////////////////////
    }

    keyPressNumber(evt) {
        var keyCode = (evt.which) ? evt.which : evt.keyCode
        if (keyCode > 47 && keyCode < 58 || keyCode == 8 || keyCode == 9)
            return true;
        return false;
    }

    validateShippingNo(i) {
        var shippingBillNumbers = $('.shipibillno' + i).val();
        if (/^(?!0{7})\d{7}$/.test(shippingBillNumbers)) {
            this.saveBtnDisable = false;
        }
        else {
            this.saveBtnDisable = true;
        }
    }
    saveSealDetail() {
        this.sealNo = this.sealNo.toUpperCase();
        var time = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/;
        this.sealingDate = $('#sealingDateOne').val();
        this.confirmSealingDate = this.sealingDate.split("-");
        this.confirmSealingDate = '' + this.confirmSealingDate[2] + '-' + this.confirmSealingDate[1] + '-' + this.confirmSealingDate[0];
        var regContainerNo = /^[a-zA-Z]{4}[0-9]{7}$/;

        if (!regContainerNo.test(this.containerNo)) {

            this.alertDanger = true;
            this.errorMsg = "Container number should start with 4 alphabates and 7 digits";
            $(".alert-danger").fadeIn('fast');
            setTimeout(function () {
                $(".alert-danger").fadeOut(2000);
            }, 4000);
        }
        else if (!time.test(this.sealingTime)) {
            this.alertDanger = true;
            this.errorMsg = "Sealing TIme must be HH:MM:SS";
            $(".alert-danger").fadeIn('fast');
            setTimeout(function () {
                $(".alert-danger").fadeOut(2000);
            }, 4000);
        }

        else if (this.selectedItemsEditIcdPort.length == 0) {
            this.alertDanger = true;
            this.errorMsg = "Please select Port or ICDs";
            $(".alert-danger").fadeIn('fast');
            setTimeout(function () {
                $(".alert-danger").fadeOut(2000);
            }, 4000);
        }
        else {
            this.paginationLoader = true;
            $("#check_container").hide();
            this.containerNo = this.containerNo.toUpperCase();
            this.truckNo = this.truckNo.toUpperCase();
            let editSealDetails = {
                "sealNo": this.sealNo,
                "sealingDate": this.confirmSealingDate,
                "shippingBillNo": this.shippingBillNo,
                "sealingTime": this.sealingTime,
                "containerNo": this.containerNo,
                "truckNo": this.truckNo,
                "shippingBillDate": this.shippingBillDate,
                "destinationStation": this.editIcdPort,
            }            
            this.masterService.editSealDetails(editSealDetails)
                .subscribe(
                    (data) => {
                        this.paginationLoader = false;
                        this.alertSuccess = true;                        
                        this.editBtnShow = true;
                        this.saveBtnShow = false;
                        this.successMsg = data["message"];
                        $(".alert-success").fadeIn('fast');
                        setTimeout(function () {
                            $(".alert-success").fadeOut(2000);
                        }, 4000);
                        this.searchSealDetails();
                    },
                    (error) => {
                        this.paginationLoader = false;
                        this.alertDanger = true;
                        this.errorMsg = error.error.message;
                        $(".alert-danger").fadeIn('fast');
                        setTimeout(function () {
                            $(".alert-danger").fadeOut(2000);
                        }, 4000);
                        this.searchSealDetails();
                    },
                    () => { }
                );
        }
    }

    statusChange() {
        this.paginationLoader = true;
        this.masterService.sealStatusChange(this.sealNumber)
            .subscribe(
                (data) => {
                    // console.log(data);
                    this.paginationLoader = false;
                    this.alertSuccess = true;
                    this.successMsg = data["message"];
                    $(".alert-success").fadeIn('fast');
                    setTimeout(function () {
                        $(".alert-success").fadeOut(2000);
                    }, 4000);
                    this.scanSealShow = false;
                    this.hideIconDiv = true;

                },
                (error) => {
                    this.paginationLoader = false;
                    console.log(error);
                },
                () => { }
            );
    }

    statusChangeHide() {
        this.scanSealShow = false;
        this.hideIconDiv = true;
        this.sealNumber = "";
    }

    searchSealDetails() {
        if (this.sealNumber.length == 12) {
            this.sealNumber = this.sealNumber.toUpperCase();
            // console.log(this.sealNumber);
            let seal_number_string = this.sealNumber.split(/(\d+)/);
            if (seal_number_string[0] == 'sppl') {
                this.alertDanger = true;
                this.errorMsg = "Seal number should starts with SPPL"
                $(".alert-danger").fadeIn('fast');
                setTimeout(function () {
                    $(".alert-danger").fadeOut(2000);
                }, 4000);
            }
            else if (seal_number_string[1].length != 8) {
                this.alertDanger = true;
                this.errorMsg = "Seal number should contain 8 digits"
                $(".alert-danger").fadeIn('fast');
                setTimeout(function () {
                    $(".alert-danger").fadeOut(2000);
                }, 4000);
            }
            else if (this.sealNumber.length != 12) {
                this.alertDanger = true;
                this.errorMsg = "Seal number should starts with 4 alphabetes in uppercase and 8 digits"
                $(".alert-danger").fadeIn('fast');
                setTimeout(function () {
                    $(".alert-danger").fadeOut(2000);
                }, 4000);
            }
            else {
                this.paginationLoader = true;
                this.masterService.sealDetails(this.sealNumber)
                    .subscribe(
                        (data) => {
                            this.paginationLoader = false;
                            this.icdDropdown = data["data"]["porticd"];
                            // console.log(this.icdDropdown);
                            $('.preloader').hide();
                            if (data["data"]["sealdata"][0].status == "Arrived") {
                                this.editBtnDisabled = true;
                            }
                            else {
                                this.editBtnDisabled = false;
                            }
                            if (data["data"].length != 0) {
                                this.sealDetails = data["data"]["sealdata"][0];
                                // console.log(this.sealDetails);
                                this.sealNo = data["data"]["sealdata"][0].sealNo;
                                this.sealingDate = data["data"]["sealdata"][0].sealingDate;
                                this.sealingTime = data["data"]["sealdata"][0].sealingTime;
                                this.containerNo = data["data"]["sealdata"][0].containerNo;
                                this.truckNo = data["data"]["sealdata"][0].truckNo;
                                this.shippingBillNo = data["data"]["sealdata"][0].shippingBillNo;
                                this.shippingBillNo1 = data["data"]["sealdata"][0].shippingBillNo;

                                this.shippingBillDate = data["data"]["sealdata"][0].shippingBillDate;
                                this.shippingBillDate1 = data["data"]["sealdata"][0].shippingBillDate;

                                this.getPortIcds = data["data"]["sealdata"][0].destinationStation;

                                this.array.push(this.getPortIcds);
                                this.hideSealDiv = true;
                                this.hideIconDiv = false;
                                this.showDiv = false;
                                this.scanSealShow = false;
                            }
                            else {
                                this.showDiv = true;
                                this.hideSealDiv = false;
                                this.hideIconDiv = false;
                            }
                        },
                        (error) => {
                            this.paginationLoader = false;
                            this.alertDanger = true;
                            this.errorMsg = error["error"].message;
                            $(".alert-danger").fadeIn('fast');
                            setTimeout(function () {
                                $(".alert-danger").fadeOut(2000);
                            }, 4000);
                            if (error["error"].message == "Seal is scanned before installed in software, do you want to reset") {
                                this.hideSealDiv = false;
                                this.scanSealShow = true;
                                this.hideIconDiv = false;
                            }
                            else {
                                this.scanSealShow = false;
                                this.hideIconDiv = true;
                            }
                        },
                        () => { }
                    );
            }
        }
        else {
            this.paginationLoader = false;
            this.alertDanger = true;
            this.errorMsg = "Seal number should starts with 4 alphabetes in uppercase and 8 digits"
            $(".alert-danger").fadeIn('fast');
            setTimeout(function () {
                $(".alert-danger").fadeOut(2000);
            }, 4000);
        }
    }

}
