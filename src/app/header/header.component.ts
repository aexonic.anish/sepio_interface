import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MasterService } from '../services/master-service.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router, private masterService: MasterService) { }

  ngOnInit() {
  }

  gotoHome() {
    this.router.navigate(['seal-details'])
  }

  gotoCustomNotification() {
    this.router.navigate(['custom-notification'])
  }


  logout() {
    this.masterService.removeToken();
    this.router.navigate(['/login']);
  }


}
