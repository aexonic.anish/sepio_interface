import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SealDetailsComponent } from './seal-details/seal-details.component';
import { CustomNotificationComponent } from './custom-notification/custom-notification.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AuthGuard } from './services/authguard-service.service';

const routes: Routes = [
      {
        path: '',
        redirectTo: '/login',
        pathMatch: 'full'
      },
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'seal-details',
        component: SealDetailsComponent,
        canActivate: [ AuthGuard ]
      },
      {
        path: 'custom-notification',
        component: CustomNotificationComponent,
        canActivate: [ AuthGuard ]
      },
      { 
        path: '**', 
        component: PageNotFoundComponent
      },
]

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}