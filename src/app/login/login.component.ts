import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MasterService } from '../services/master-service.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})    
export class LoginComponent implements OnInit {

    public email;
    public password;
    public successMsg;
    public errorMsg;
    public alertSuccess: boolean = false;
    public alertDanger: boolean = false;
    public paginationLoader: boolean = false;

  constructor(private masterService: MasterService, private router: Router) {
        if(this.masterService.isLoggedIn())
        {
            let token = this.masterService.getToken();
            let decryptedToken = JSON.parse(window.atob(token.split('.')[1]));
            if(decryptedToken.userTypeId == 1 )
            {
                this.router.navigate(['/seal-details']);
            }
        }
   }

  ngOnInit() {
  }

  login()
  {
    this.paginationLoader = true;
    let loginData = {
                      "username": this.email,
                      "password": this.password 
                    }
    this.masterService.userLogin(loginData)
    .subscribe(
                  (data) => {
                              let decryptedToken = JSON.parse(window.atob(data["token"].split('.')[1]));
                              this.masterService.setToken(data["token"]); 

                              if(decryptedToken.userTypeId == 1 )
                              {       
                                  this.router.navigate(['/seal-details']);
                              }
                              else
                              {
                                  this.paginationLoader = false;
                                  this.alertDanger = true;
                                  this.errorMsg = "Invalid Credentials"
                                  $(".alert-danger").fadeIn('fast');
                                  setTimeout(function() {
                                      $(".alert-danger").fadeOut(2000);
                                  }, 4000);
                                  this.router.navigate(['/login']);
                              }
                            },
                  (error) => {
                                this.paginationLoader = false;
                                this.alertDanger = true;
                                this.errorMsg = error.error.message;
                                $(".alert-danger").fadeIn('fast');
                                setTimeout(function() {
                                    $(".alert-danger").fadeOut(2000);
                                }, 4000);
                            },
                  () => {}
              );
  }

}
