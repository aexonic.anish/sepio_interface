import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { Daterangepicker } from 'ng2-daterangepicker';
import { HttpModule } from "@angular/http";
import { AppComponent } from './app.component';
import { AuthGuard } from './services/authguard-service.service';
import { MasterService } from './services/master-service.service';

import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';
import { SealDetailsComponent } from './seal-details/seal-details.component';
import { CustomNotificationComponent } from './custom-notification/custom-notification.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SealDetailsComponent,
    CustomNotificationComponent,
    HeaderComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    AngularMultiSelectModule,
    Daterangepicker,
  ],
  providers: [ HttpModule, HttpClientModule, AuthGuard, MasterService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
