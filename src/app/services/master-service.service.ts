import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class MasterService {

	public options: any = {};
	// public api_url = "https://gstsealbackend-test.herokuapp.com/";
	// public api_url = "http://192.168.1.17:3000/";
	public api_url = "https://gstsealbackend.herokuapp.com/";


	constructor(private http: HttpClient, private router: Router) {
	}

	getCustomsIcd() {
		this.options = this.getHeader();
		let data = this.http.get(this.api_url + 'customsExecutive/customsicdlist', this.options)
		return data;
	}

	sendCustomNotificationData(customNotification) {
		this.options = this.getHeader();
		let data = this.http.post(this.api_url + 'sepio/customnotification', customNotification, this.options)
		return data;
	}

	userLogin(loginData) {
		this.options = this.getHeader();
		let data = this.http.post(this.api_url + 'users/login', loginData, this.options)
		return data;
	}

	sealDetails(searchText) {
		this.options = this.getHeader();
		let data = this.http.get(this.api_url + 'sepioInterface/searchaseal?searchString=' + searchText + '&pageNo=0', this.options)
		return data;
	}

	editSealDetails(editSealDetails) {
		this.options = this.getHeader();
		let data = this.http.post(this.api_url + 'sepioInterface/updatesearchseal', editSealDetails, this.options)
		return data;
	}

	sealStatusChange(sealNumber) {
		let sealdata = {
			"sealNo": sealNumber,
		}
		this.options = this.getHeader();
		let data = this.http.post(this.api_url + 'sepioInterface/resetaseal', sealdata, this.options)
		return data;
	}

	getCompanyData() {
		this.options = this.getHeader();
		let data = this.http.get(this.api_url + 'sepio/companylist', this.options)
		return data;
	}

	getDistributorData() {
		this.options = this.getHeader();
		let data = this.http.get(this.api_url + 'distributorModule/activedistributerdata', this.options)
		return data;
	}
	getCompanyViaDisData() {
		this.options = this.getHeader();
		let data = this.http.get(this.api_url + 'sepio/companydislist', this.options)
		return data;
	}
	getCustomsData() {
		this.options = this.getHeader();
		let data = this.http.get(this.api_url + 'sepio/customlist', this.options)
		return data;
	}

	setToken(token) {
		localStorage.setItem("token", token);
	}

	getToken() {
		return localStorage.getItem("token");
	}

	removeToken() {
		localStorage.removeItem("token");
		this.router.navigate(['/login']);
	}

	getHeader() {
		let headers = new Headers();
		headers.append('Accept', 'application/json');
		return headers;
	}

	isLoggedIn() {
		let token = this.getToken();
		if (token) {
			let payload = JSON.parse(window.atob(token.split('.')[1]));
			var currentDate1 = Date.now();
			var currentDate = Math.floor(currentDate1 / 1000);
			if (currentDate >= payload.exp) {
				this.router.navigate(['/login']);
			}
			else {
				return true;
			}
		}
		else {
			this.router.navigate(['/login']);
		}
	}

}
