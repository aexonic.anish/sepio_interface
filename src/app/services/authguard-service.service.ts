import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { MasterService } from '../services/master-service.service';
import { Router } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

	constructor(private router: Router, private masterService: MasterService) {}

	canActivate() 
	{
		if ((this.masterService.isLoggedIn())) 
		{
			let token = this.masterService.getToken();
			let decryptedToken = JSON.parse(window.atob(token.split('.')[1]));
			return true;
		}
		else
		{
			// console.log("canactivate not done");
			if(this.router.url != '/')
			{
				return true;
			}
			else
			{
				this.router.navigate(['/login']);
			}			
		}
		return false;
	}
}